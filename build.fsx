// include Fake lib
#r @"packages/FAKE/tools/FakeLib.dll"
open Fake
open System.Diagnostics
open System.IO
open System


let slnFiles = ["FilterDrivers.sln"]
let debugDir = "output/debug"
let remoteDebugDir = "Z:/FOXDIE/debug"
let debugFiles = [
    "deployment/diagnostics.bat"
    "deployment/drivers.reg"
    "deployment/install.bat"
    "deployment/uninsthid.reg"
    "deployment/uninstall.bat"
    "deployment/WdfCoinstaller01009.dll"
    "kbfiltr/x64/Debug/hidkeyboard.sys"
    "moufiltr/x64/Debug/hidmouse.sys"
]
let releaseDir = "output/release"
let remoteReleaseDir = "Z:/FOXDIE/release"
let releaseFiles = [
    "deployment/diagnostics.bat"
    "deployment/drivers.reg"
    "deployment/install.bat"
    "deployment/uninsthid.reg"
    "deployment/uninstall.bat"
    "deployment/WdfCoinstaller01009.dll"
    "kbfiltr/x64/Release/hidkeyboard.sys"
    "moufiltr/x64/Release/hidmouse.sys"
]

Target "Clean" (fun _ ->
    trace "[CLEAN] Cleaning all solution files."
    slnFiles
    |> MSBuildDebug "" "Clean" 
    |> Log "Clean output:"
    DeleteDir debugDir
    DeleteDir releaseDir
)

Target "Debug" (fun _ ->
    slnFiles
    |> MSBuild "" "" [ ("Configuration", "Debug"); ("Platform", "x64"); ("OutFolder", "output_debug")]
    |> Log "Build output:"
)

Target "Release" (fun _ ->
    slnFiles
    |> MSBuild "" "" [ ("Configuration", "Release"); ("Platform", "x64")]
    |> Log "Build output:"
)


Target "Package" (fun _ ->
    trace "[PACKAGE] Creating installation packages."
    if allFilesExist debugFiles then
        DeleteDir debugDir
        trace (sprintf "[PACKAGE] Copying debug package to %s." debugDir)
        ensureDirExists (DirectoryInfo(debugDir))
        Copy debugDir debugFiles
    else
        trace "[PACKAGE] No debug package created."

    if allFilesExist releaseFiles then
        DeleteDir releaseDir
        trace (sprintf "[PACKAGE] Copying release package to %s." releaseDir)
        ensureDirExists (DirectoryInfo(releaseDir))
        Copy releaseDir releaseFiles
    else
        trace "[PACKAGE] No release package created."
)

Target "Deploy" (fun _ ->
    // CopyDir remoteDebugDir debugDir (fun _ -> true)
    // CopyDir remoteReleaseDir releaseDir (fun _ -> true)
    trace "[DEPLOY] Running all files named \"deploy.bat\" in folder subtree."
    directoryInfo "."
    |> filesInDirMatchingRecursive "deploy.bat"
    |> Array.iter (fun fi -> 
        trace (sprintf "[DEPLOY] %s" fi.FullName)
        (Shell.Exec fi.FullName) |> ignore
        )
)

// "Default" ==> "Deploy"
// start build
RunTargetOrDefault "Debug"
