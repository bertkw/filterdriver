﻿open OeMedia
open System.Threading
open System
open System.Diagnostics

let initOeMediaDriver () =
    try
        let oemDrv = new KbDrv(),new MouDrv()
        Some(oemDrv)
    with
    | e -> 
        printfn "%s\n%s" e.Message e.StackTrace 
        None

let KEY_MAKE = 0us
let KEY_BREAK = 1us
let sendKey kbDrv c = (kbDrv:KbDrv).SendKey(c, 60, 40)
let sendKeys kbDrv s = (s:string).ToCharArray() |> Array.iter(sendKey kbDrv)
[<EntryPoint>]
let main argv = 
    printfn "Initializing driver..."
    let driverOpt = initOeMediaDriver()
    match driverOpt with
    | Some (kbDrv, mouDrv) ->
        printfn "Drivers successfully initialized."
        printfn "Sleeping for 2 seconds..."
        
        printfn "Starting notepad.exe."
        Process.Start("notepad.exe") |> ignore
        printfn "Sleeping for 2 seconds..."
        Thread.Sleep(2000)
        printfn "Sending keys to notepad."
        kbDrv.SendEnterKey()
        sendKeys kbDrv "None of this is injected."
        kbDrv.SendEnterKey()
        kbDrv.KeyDown(0xBDus)
        Thread.Sleep(50)
        kbDrv.KeyUp(0xBDus)

        printfn "Sending right mouse click."
        // mouDrv.RightButtonDown().Sleep(75).RightButtonUp() |> ignore 

        for i in 0..50 do
            mouDrv.MouseScrollDown().Sleep(50) |> ignore

        printfn "Opening start menu."
        kbDrv.GenerateKbdEvent(0x5Bus, (KbDrv.KEY_MAKE ||| KbDrv.KEY_E0));
        // kbDrv.SendKey('r', 30, 30);
        Thread.Sleep(1000);
        kbDrv.GenerateKbdEvent(0x5Bus, (KbDrv.KEY_BREAK ||| KbDrv.KEY_E0));
        //kbDrv.KeyUp(0x45us);
        kbDrv.KeyDown(0xBDus)
        Thread.Sleep(50)
        kbDrv.KeyUp(0xBDus)
        kbDrv.KeyDown(0xBDus)
        Thread.Sleep(50)
        kbDrv.KeyUp(0xBDus)

        kbDrv.Dispose()
        mouDrv.Dispose()
    | None -> printfn "Driver initialization failed."
    0 // return an integer exit code
