﻿module Hook

open System
open System.Runtime.InteropServices
open InputEventViewer
open InputEventViewer.Constants
open System.Windows.Forms

// http://www.pinvoke.net/default.aspx/Structures/KBDLLHOOKSTRUCT.html
// https://gist.github.com/Stasonix/3181083#file-form1-cs-L63
// https://msdn.microsoft.com/en-us/library/windows/desktop/ms644967(v=vs.85).aspx KBDLLHOOKSTRUCT
// https://msdn.microsoft.com/en-us/library/windows/desktop/ms644985(v=vs.85).aspx LowLevelKeyboardProc

// reason for hooking on user32: http://stackoverflow.com/questions/11607133/global-mouse-event-handler

// http://stackoverflow.com/questions/3847842/lowlevelmouseproc-in-background-thread

// http://www.pinvoke.net/default.aspx/Structures/MSLLHOOKSTRUCT.html
// http://www.pinvoke.net/default.aspx/Delegates/LowLevelMouseProc.html
 
[<DllImport("user32.dll")>]
extern IntPtr SetWindowsHookEx(int idHook, LowLevelInputProc callback, IntPtr hInstance, uint32 threadId);

[<DllImport("user32.dll")>]
extern bool UnhookWindowsHookEx(IntPtr hInstance);

[<DllImport("user32.dll")>]
extern IntPtr CallNextHookEx(IntPtr idHook, int nCode, int wParam, IntPtr lParam);

[<DllImport("kernel32.dll")>]
extern IntPtr LoadLibrary(string lpFileName);

type KeyboardHook(hookFn) = 
    let mutable hHook = IntPtr()
    let hookKeyboardProc (fn:IntPtr -> KBDLLHOOKSTRUCT -> unit) (code:int) (wParam:IntPtr) (lParam:IntPtr) : IntPtr = 
        //MessageBox.Show (sprintf "hHook:[%d] code:[%d]" (hHook.ToInt64()) code) |> ignore
        if code >= 0 then
            //MessageBox.Show "marshalling fn!" |> ignore
            try
                let kbStruct = Marshal.PtrToStructure(lParam, typeof<KBDLLHOOKSTRUCT>) :?> KBDLLHOOKSTRUCT
                fn wParam kbStruct
            with
            | e -> MessageBox.Show (e.Message + "\r\n" + e.StackTrace) |> ignore
        CallNextHookEx(hHook, code, wParam.ToInt32(), lParam)

    let setKbdHook fn =
        let hInstance = LoadLibrary("User32")
        let llkp = LowLevelInputProc(hookKeyboardProc fn)
        hHook <- SetWindowsHookEx(WH_KEYBOARD_LL, llkp, hInstance, 0u)

    let unhookKbdHook () = UnhookWindowsHookEx(hHook)

    do 
        setKbdHook hookFn
        // MessageBox.Show "kb hook created!" |> ignore

    let mutable disposed = false
    interface IDisposable with
        member this.Dispose() = 
            if not disposed then
                unhookKbdHook() |> ignore
            disposed <- true
    
type MouseHook(hookFn) = 
    let mutable hHook = IntPtr()            
    let hookMouseProc (fn:IntPtr -> MSLLHOOKSTRUCT -> unit) (code:int) (wParam:IntPtr) (lParam:IntPtr) : IntPtr = 
        if code >= 0 then
            let msStruct = Marshal.PtrToStructure(lParam, typeof<MSLLHOOKSTRUCT>) :?> MSLLHOOKSTRUCT
            fn wParam msStruct
        CallNextHookEx(hHook, code, wParam.ToInt32(), lParam)

    let setMsHook fn =
        let hInstance = LoadLibrary("User32")
        let llmp = LowLevelInputProc(hookMouseProc fn)
        hHook <- SetWindowsHookEx(WH_MOUSE_LL, llmp, hInstance, 0u)

    let unhookMsHook () = UnhookWindowsHookEx(hHook)

    do setMsHook hookFn

    let mutable disposed = false
    interface IDisposable with
        member this.Dispose() = 
            if not disposed then
                unhookMsHook() |> ignore
            disposed <- true

let isKbdEventInjected (kbdLlStruct:KBDLLHOOKSTRUCT) = 
    (kbdLlStruct.KBDLLHOOKSTRUCTFlags &&& (uint32 KBDLLHOOKSTRUCTFlags.LLKHF_INJECTED)) <> 0u

let isMsEventInjected (msLlStruct:MSLLHOOKSTRUCT) = 
    (msLlStruct.Flags &&& (uint32 MSLLHOOKSTRUCTFlags.LLKHF_INJECTED)) <> 0u