﻿namespace InputEventViewer

open System.Windows.Forms

type InputEventViewerForm() as thisForm = 
    inherit Form()

    let panel1 = new System.Windows.Forms.Panel()
    let inputEventsTB = new System.Windows.Forms.TextBox()

    let init() = 
        panel1.SuspendLayout()
        thisForm.SuspendLayout()
        // 
        // panel1
        // 
        panel1.Controls.Add inputEventsTB
        panel1.Dock <- System.Windows.Forms.DockStyle.Fill
        panel1.Location <- new System.Drawing.Point(0, 0)
        panel1.Name <- "panel1"
        panel1.Size <- new System.Drawing.Size(800, 773)
        panel1.TabIndex <- 0
        // 
        // inputEventsTB
        // 
        inputEventsTB.BackColor <- System.Drawing.Color.White
        inputEventsTB.ForeColor <- System.Drawing.Color.Black
        inputEventsTB.Dock <- System.Windows.Forms.DockStyle.Fill
        inputEventsTB.Font <- new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)))
        inputEventsTB.Location <- new System.Drawing.Point(0, 0)
        inputEventsTB.MaxLength <- 3276700
        inputEventsTB.Multiline <- true
        inputEventsTB.Name <- "inputEventsTB"
        inputEventsTB.ReadOnly <- true
        inputEventsTB.Size <- new System.Drawing.Size(600, 773)
        inputEventsTB.TabIndex <- 0
        // 
        // KbdHookForm
        // 
        thisForm.AutoScaleDimensions <- new System.Drawing.SizeF(6.0F, 13.0F)
        thisForm.AutoScaleMode <- System.Windows.Forms.AutoScaleMode.Font
        thisForm.ClientSize <- new System.Drawing.Size(800, 773)
        thisForm.Controls.Add(panel1)
        thisForm.Name <- "KbdHookForm"
        thisForm.Text <- "KbdHook"
        panel1.ResumeLayout(false)
        panel1.PerformLayout()
        thisForm.ResumeLayout(false)

    do init()

    member this.Panel = panel1
    member this.InputEventsTB = inputEventsTB
