﻿#nowarn "9"
namespace InputEventViewer

open System
open System.Runtime.InteropServices

type LowLevelInputProc = delegate of int * IntPtr * IntPtr -> IntPtr

[<System.FlagsAttribute>]
type KBDLLHOOKSTRUCTFlags = 
| LLKHF_EXTENDED = 0x01u
| LLKHF_INJECTED = 0x10u
| LLKHF_ALTDOWN = 0x20u
| LLKHF_UP = 0x80u

[<type:StructLayout(LayoutKind.Sequential)>]
type KBDLLHOOKSTRUCT = 
    struct 
        val VkCode:uint32
        val ScanCode:uint32
        val KBDLLHOOKSTRUCTFlags:uint32
        val Time:uint32
        val DwExtraInfo:IntPtr
    end

[<type:StructLayout(LayoutKind.Sequential)>]
type POINT = 
    val X:int
    val Y:int

[<System.FlagsAttribute>]
type MSLLHOOKSTRUCTFlags = 
| LLKHF_INJECTED = 0x1
| LLKHF_LOWER_IL_INJECTED = 0x2

[<type:StructLayout(LayoutKind.Sequential)>]
type MSLLHOOKSTRUCT = 
    struct
        val Pt:POINT
        val MouseData:uint32
        val Flags:uint32
        val Time:uint32
        val DwExtraInfo:IntPtr
    end

module Constants = 
    [<Literal>]
    let WH_KEYBOARD_LL = 13
    [<Literal>]
    let WM_KEYDOWN = 0x100
    [<Literal>]
    let WM_KEYUP = 0x101

    [<Literal>]
    let WH_MOUSE_LL = 14
    [<Literal>]
    let WM_LBUTTONDOWN = 0x0201
    [<Literal>]
    let WM_LBUTTONUP = 0x0202
    [<Literal>]
    let WM_MOUSEMOVE = 0x0200
    [<Literal>]
    let WM_MOUSEWHEEL = 0x020A
    [<Literal>]
    let WM_RBUTTONDOWN = 0x0204
    [<Literal>]
    let WM_RBUTTONUP = 0x0205
