﻿open System.Windows.Forms
open InputEventViewer
open System
open InputEventViewer.Constants
open Hook

type LoopEvent = 
| Keyboard of string
| Mouse of string

let startEventAgent (form:InputEventViewerForm) = MailboxProcessor.Start (fun inbox ->
    let rec loop () = async {
        let! msg = inbox.Receive()
        match msg with
        | Keyboard str -> let s = sprintf "[KEYBOARD][%s] %s" (DateTime.Now.ToString("HH:mm:ss")) str
                          form.InputEventsTB.AppendText(s + "\r\n")
        | Mouse str -> let s = sprintf "[MOUSE***][%s] %s" (DateTime.Now.ToString("HH:mm:ss")) str
                       form.InputEventsTB.AppendText(s + "\r\n")
        return! loop()
    }
    loop())

let kc = new KeysConverter()
let kbdEventHandler (mbp:MailboxProcessor<LoopEvent>) (msg:IntPtr) (kbdLlStruct:KBDLLHOOKSTRUCT) =
    // if isKbdEventInjected kbdLlStruct then
    if true then
        let msgInt = msg.ToInt32()
        let wmStr = 
            match msgInt with
            | WM_KEYDOWN -> "WM_KEYDOWN"
            | WM_KEYUP -> "WM_KEYUP"
            | _ -> "UNKNOWN"
        let str = sprintf "EVENT:[%s] VkCode:[%d] Injected:[%s]" wmStr kbdLlStruct.VkCode ((isKbdEventInjected kbdLlStruct).ToString())
        mbp.Post (Keyboard str) 

let msEventHandler (mbp:MailboxProcessor<LoopEvent>) (msg:IntPtr) (msLlStruct:MSLLHOOKSTRUCT) =
    // if isMsEventInjected msLlStruct then
    if true then
        let msgInt = msg.ToInt32()
        let mwStrOpt = 
            match msgInt with
            | WM_LBUTTONDOWN -> Some "WM_LBUTTONDOWN"
            | WM_LBUTTONUP -> Some "WM_LBUTTONUP"
            | WM_RBUTTONDOWN -> Some "WM_RBUTTONDOWN"
            | WM_RBUTTONUP -> Some "WM_RBUTTONUP"
            | WM_MOUSEMOVE -> None // Some "WM_MOUSEMOVE"
            | WM_MOUSEWHEEL -> Some "WM_MOUSEWHEEL"
            | _ -> Some "UNKNOWN"
        match mwStrOpt with
        | None -> ()
        | Some mwStr -> 
            let str = sprintf "EVENT:[%s] Point:[%d,%d] Injected:[%s]" mwStr msLlStruct.Pt.X msLlStruct.Pt.Y 
                                                                ((isMsEventInjected msLlStruct).ToString())
            mbp.Post (Mouse str)

[<EntryPoint>]
let main argv = 
    Application.EnableVisualStyles();
    Application.SetCompatibleTextRenderingDefault(false);

    let ievf = new InputEventViewerForm()
    let eventAgent = startEventAgent ievf

    let kbHook = new KeyboardHook(kbdEventHandler eventAgent)
    let msHook = new MouseHook(msEventHandler eventAgent)

    ievf.Closing |> Observable.add (fun e ->
        (kbHook :> IDisposable).Dispose()
        (msHook :> IDisposable).Dispose()
    )

    // ievf.InputEventsTB.AppendText ("Hello!\r\n")
    Application.Run(ievf);
    0 // return an integer exit code
