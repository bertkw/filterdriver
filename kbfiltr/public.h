#ifndef _KBFILTR_PUBLIC_H
#define _KBFILTR_PUBLIC_H

#define IOCTL_INDEX_GEN_KEY_EVENT						0x852

#define IOCTL_KBFILTR_GEN_KEY_EVENT		CTL_CODE( FILE_DEVICE_KEYBOARD,   \
                                                        IOCTL_INDEX_GEN_KEY_EVENT,    \
                                                        METHOD_BUFFERED,    \
                                                        FILE_ANY_ACCESS)

typedef struct KeypressData_t {
	USHORT ScanCode;
	USHORT Flags;
} KeypressData;
#endif
