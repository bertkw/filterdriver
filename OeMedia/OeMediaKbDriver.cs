﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OeMedia
{
    public class KbDrv : IDisposable
    {
        public const ushort KEY_MAKE = 0;
        public const ushort KEY_BREAK = 1;
        public const ushort KEY_E0 = 2;
        public const ushort KEY_E1 = 4;

        private IntPtr hDriver;
        public KbDrv()
        {
            hDriver = Driver.OpenKeyboardDevice();
        }

        // Generate scancodes using win32api::MapVirtualKey()
        public void GenerateKbdEvent(ushort scanCode, ushort flags)
        {
            Driver.SendKeypressData(hDriver, scanCode, flags);
        }

        public void KeyDown(ushort scanCode)
        {
            GenerateKbdEvent(scanCode, KEY_MAKE);
        }

        public void KeyUp(ushort scanCode)
        {
            GenerateKbdEvent(scanCode, KEY_BREAK);
        }

        [DllImport("user32.dll")]
        private static extern uint MapVirtualKey(uint uCode, uint uMapType);
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        private static extern short VkKeyScan(char ch);
        public void SendKey(char ch, int keywait, int keymodwait)
        {
            short vk = VkKeyScan(ch);
            byte upperVk = (byte)(vk >> 8);
            byte lowerVk = (byte)(vk & 0xff);

            bool shift = upperVk > 0;
            byte c = lowerVk;
            uint scancode = MapVirtualKey(c, 0);

            if (shift)
            {
                KeyDown(0x2A);
                Thread.Sleep(keymodwait >> 1);
            }

            KeyDown((ushort)scancode);

            if (keywait > 0)
                Thread.Sleep(keywait);

            KeyUp((ushort)scancode);

            if (shift)
            {
                Thread.Sleep(keymodwait >> 1);
                KeyUp(0x2A);
            }
        }

        public void SendEnterKey(int keywait = 67)
        {
            KeyDown(0x9C);

            if (keywait > 0)
                Thread.Sleep(keywait);

            KeyUp(0x9C);
        }

        private bool _disposed = false;
        public void Dispose()
        {
            if (!_disposed)
            {
                Driver.CloseDriverHandle(hDriver);
                hDriver = IntPtr.Zero;
                _disposed = true;
            }
        }
    }
}
