﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OeMedia
{
    public class MouDrv : IDisposable
    {
        public const ushort BF_MOUSE_LEFT_BUTTON_DOWN = 0x0001;  // Left Button changed to down.
        public const ushort BF_MOUSE_LEFT_BUTTON_UP = 0x0002;  // Left Button changed to up.
        public const ushort BF_MOUSE_RIGHT_BUTTON_DOWN = 0x0004;  // Right Button changed to down.
        public const ushort BF_MOUSE_RIGHT_BUTTON_UP = 0x0008;  // Right Button changed to up.
        public const ushort BF_MOUSE_MIDDLE_BUTTON_DOWN = 0x0010;  // BF_Middle Button changed to down.
        public const ushort BF_MOUSE_MIDDLE_BUTTON_UP = 0x0020;  // BF_Middle Button changed to up.

        // Setting ButtonData with the MOUSE_WHEEL flag set:
        // http://www.osronline.com/showThread.CFM?link=111732
        // https://msdn.microsoft.com/en-us/library/ms997498.aspx
 
        public const ushort BF_MOUSE_WHEEL = 0x0400;

        public const ushort BF_MOUSE_BUTTON_1_DOWN = BF_MOUSE_LEFT_BUTTON_DOWN;
        public const ushort BF_MOUSE_BUTTON_1_UP = BF_MOUSE_LEFT_BUTTON_UP;
        public const ushort BF_MOUSE_BUTTON_2_DOWN = BF_MOUSE_RIGHT_BUTTON_DOWN;
        public const ushort BF_MOUSE_BUTTON_2_UP = BF_MOUSE_RIGHT_BUTTON_UP;
        public const ushort BF_MOUSE_BUTTON_3_DOWN = BF_MOUSE_MIDDLE_BUTTON_DOWN;
        public const ushort BF_MOUSE_BUTTON_3_UP = BF_MOUSE_MIDDLE_BUTTON_UP;

        public const ushort MOUSE_MOVE_RELATIVE = 0;
        public const ushort MOUSE_MOVE_ABSOLUTE = 1;
        public const ushort MOUSE_VIRTUAL_DESKTOP = 0x02;  // the coordinates are mapped to the virtual desktop
        // public const ushort MOUSE_ATTRIBUTES_CHANGED = 0x04;  // requery for mouse attributes

        private IntPtr hDriver;
        public MouDrv()
        {
            hDriver = Driver.OpenMouseDevice();
        }

        private void GenerateMouEvent(ushort buttonFlags, ushort buttonData = 0)
        {
            Driver.SendMouseEventData(hDriver, buttonFlags, buttonData, MOUSE_MOVE_RELATIVE, 0, 0);
        }

        private void GenerateMouEvent(ushort buttonFlags, ushort buttonData, ushort flags, int lastX, int lastY)
        {
            Driver.SendMouseEventData(hDriver, buttonFlags, buttonData, flags, lastX, lastY);
        }

        public MouDrv LeftButtonDown()
        {
            GenerateMouEvent(BF_MOUSE_LEFT_BUTTON_DOWN);
            return this;
        }

        public MouDrv LeftButtonUp()
        {
            GenerateMouEvent(BF_MOUSE_LEFT_BUTTON_UP);
            return this;
        }

        public MouDrv RightButtonDown()
        {
            GenerateMouEvent(BF_MOUSE_RIGHT_BUTTON_DOWN);
            return this;
        }

        public MouDrv RightButtonUp()
        {
            GenerateMouEvent(BF_MOUSE_RIGHT_BUTTON_UP);
            return this;
        }

        public MouDrv MiddleButtonDown()
        {
            GenerateMouEvent(BF_MOUSE_MIDDLE_BUTTON_DOWN);
            return this;
        }

        public MouDrv MiddleButtonUp()
        {
            GenerateMouEvent(BF_MOUSE_MIDDLE_BUTTON_UP);
            return this;
        }

        public MouDrv Sleep(int ms)
        {
            Thread.Sleep(ms);
            return this;
        }

        public MouDrv MouseScrollDown()
        {
            ushort wheelDelta = unchecked((ushort)(-120));
            GenerateMouEvent(BF_MOUSE_WHEEL, wheelDelta);
            return this;
        }

        public MouDrv MouseScrollUp()
        {
            ushort wheelDelta = 120;
            GenerateMouEvent(BF_MOUSE_WHEEL, wheelDelta);
            return this;
        }

        private bool _disposed = false;
        public void Dispose()
        {
            if (!_disposed)
            {
                Driver.CloseDriverHandle(hDriver);
                hDriver = IntPtr.Zero;
                _disposed = true;
            }
        }

        private Random _rand = new Random();
        /// <summary>
        /// Generate random integer smaller than r.
        /// </summary>
        private int Rand(int r)
        {
            return _rand.Next(r);
        }
        /// <summary>
        /// Generates a random integer r such that low &lt;= r &lt;= high
        /// </summary>
        private int RandRange(int low, int high)
        {
            return low + _rand.Next(high - low + 1);
        }
    }
}
