﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;

namespace OeMedia
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct KeypressData
    {
        public ushort ScanCode;
        public ushort Flags;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct MouseEventData
    {
        public ushort ButtonFlags;
        public ushort ButtonData;
        public ushort Flags;
        public int LastX;
        public int LastY;
    }

    internal static class Driver
    {
        [StructLayout(LayoutKind.Sequential)]
        private struct SP_DEVICE_INTERFACE_DATA
        {
            public Int32 cbSize;
            public Guid interfaceClassGuid;
            public Int32 flags;
            private UIntPtr reserved;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto, Pack = 1)]
        private struct SP_DEVICE_INTERFACE_DETAIL_DATA
        {
            public int size;
            public char devicePath;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct SP_DEVINFO_DATA
        {
            public uint cbSize;
            public Guid classGuid;
            public uint devInst;
            public IntPtr reserved;
        }

        [DllImport("setupapi.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SetupDiGetClassDevs(ref Guid ClassGuid, IntPtr Enumerator, IntPtr hwndParent, int Flags);

        [DllImport(@"setupapi.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern Boolean SetupDiEnumDeviceInterfaces(IntPtr hDevInfo, IntPtr devInfo, ref Guid interfaceClassGuid, UInt32 memberIndex, ref SP_DEVICE_INTERFACE_DATA deviceInterfaceData);

        [DllImport(@"setupapi.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern Boolean SetupDiGetDeviceInterfaceDetail(IntPtr hDevInfo, ref SP_DEVICE_INTERFACE_DATA deviceInterfaceData, ref SP_DEVICE_INTERFACE_DETAIL_DATA deviceInterfaceDetailData, UInt32 deviceInterfaceDetailDataSize, ref UInt32 requiredSize, IntPtr handle);
        [DllImport(@"setupapi.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern Boolean SetupDiGetDeviceInterfaceDetail(IntPtr hDevInfo, ref SP_DEVICE_INTERFACE_DATA deviceInterfaceData, IntPtr handle, UInt32 deviceInterfaceDetailDataSize, ref UInt32 requiredSize, IntPtr handle2);

        [DllImport("setupapi.dll", SetLastError = true)]
        private static extern bool SetupDiDestroyDeviceInfoList(IntPtr DeviceInfoSet);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CreateFile(
            [MarshalAs(UnmanagedType.LPTStr)] string filename,
            [MarshalAs(UnmanagedType.U4)] FileAccess access,
            [MarshalAs(UnmanagedType.U4)] FileShare share,
            IntPtr securityAttributes, // optional SECURITY_ATTRIBUTES struct or IntPtr.Zero
            [MarshalAs(UnmanagedType.U4)] FileMode creationDisposition,
            [MarshalAs(UnmanagedType.U4)] FileAttributes flagsAndAttributes,
            IntPtr templateFile);

        [DllImport("kernel32.dll", ExactSpelling = true, SetLastError = true, CharSet = CharSet.Auto)]
        private static extern bool DeviceIoControl(IntPtr hDevice, uint dwIoControlCode,
            IntPtr lpInBuffer, uint nInBufferSize,
            IntPtr lpOutBuffer, uint nOutBufferSize,
            out uint lpBytesReturned, IntPtr lpOverlapped);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool CloseHandle(IntPtr hObject);

        private const int DIGCF_PRESENT = 0x2;
        private const int DIGCF_DEVICEINTERFACE = 0x10;
        private static readonly Guid KBDRIVER_GUID = Guid.Parse("{7B9C8FC9-B862-4F07-855C-5D7D588BDAD3}");
        private static readonly Guid MOUDRIVER_GUID = Guid.Parse("{DFAB6BB2-6E42-4F0E-9669-4C8167192D92}");
        private static readonly uint SIZEOF_KEYPRESSDATA = (uint)Marshal.SizeOf<KeypressData>();
        private static readonly uint SIZEOF_MOUSEEVENTDATA = (uint)Marshal.SizeOf<MouseEventData>();
        private static readonly uint SIZEOF_UINT = (uint)Marshal.SizeOf<uint>();
        private const uint IOCTL_KBFILTR_GEN_KEY_EVENT = 0x000B2148;
        private const uint IOCTL_MOUFILTR_GEN_KEY_EVENT = 0x000F2048;

        /*
         *  Ported from the kbfiltr test application.
         **/
        unsafe internal static IntPtr OpenDriverDevice(Guid driverGuid)
        {
            var hHwDeviceInfo = SetupDiGetClassDevs(ref driverGuid, IntPtr.Zero, IntPtr.Zero, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
            if (hHwDeviceInfo == (IntPtr)(-1))
            {
                throw new Exception("SetupDiGetClassDevs failed.");
            }

            SP_DEVICE_INTERFACE_DATA deviceInterfaceData = new SP_DEVICE_INTERFACE_DATA();
            deviceInterfaceData.cbSize = Marshal.SizeOf<SP_DEVICE_INTERFACE_DATA>();
            uint i = 0;
            uint predictedLength = 0;
            SP_DEVICE_INTERFACE_DETAIL_DATA* devIFaceDetailData = null;
            do
            {
                if (SetupDiEnumDeviceInterfaces(hHwDeviceInfo, IntPtr.Zero, ref driverGuid, i, ref deviceInterfaceData))
                {
                    if (devIFaceDetailData != null)
                    {
                        Marshal.FreeHGlobal((IntPtr)devIFaceDetailData);
                        devIFaceDetailData = null;
                    }
                    uint requiredLength = 0;
                    if (!SetupDiGetDeviceInterfaceDetail(hHwDeviceInfo, ref deviceInterfaceData, IntPtr.Zero, 0, ref requiredLength, IntPtr.Zero))
                    {
                        if (Marshal.GetLastWin32Error() != 122L)
                        {
                            SetupDiDestroyDeviceInfoList(hHwDeviceInfo);
                            throw new Exception("SetupDiGetDeviceInterfaceDetail failed.");
                        }
                    }
                    predictedLength = requiredLength;
                    devIFaceDetailData = (SP_DEVICE_INTERFACE_DETAIL_DATA*)Marshal.AllocHGlobal((IntPtr)predictedLength);
                    // ISSUE: size was being set to 6 bytes every time (the size of the struct on 32-bit systems).  This would cause a failure in 64-bit code.
                    // FIX: check for 32-bit or 64-bit system, set size accordingly.
                    devIFaceDetailData->size = IntPtr.Size == 8 ? 8 : 4 + Marshal.SystemDefaultCharSize;

                    if (!SetupDiGetDeviceInterfaceDetail(hHwDeviceInfo, ref deviceInterfaceData, ref (*devIFaceDetailData), predictedLength, ref requiredLength, IntPtr.Zero))
                    {
                        var err = Marshal.GetLastWin32Error();
                        var msg = $"Error in SetupDiGetDeviceInterfaceDetail. (ErrorCode=[{err}]  [devIFaceDetailData is NULL]=[{devIFaceDetailData == null}]  predictedLength=[{predictedLength}]  devIFaceDetailData->size=[{devIFaceDetailData->size}]";
                        SetupDiDestroyDeviceInfoList(hHwDeviceInfo);
                        Marshal.FreeHGlobal((IntPtr)devIFaceDetailData);
                        devIFaceDetailData = null;
                        throw new Exception(msg);
                    }
                    i++;
                }
                else if (Marshal.GetLastWin32Error() != 259L)
                {
                    Marshal.FreeHGlobal((IntPtr)devIFaceDetailData);
                    devIFaceDetailData = null;
                }
                else break;
            } while (true);
            SetupDiDestroyDeviceInfoList(hHwDeviceInfo);
            if (devIFaceDetailData == null)
            {
                throw new Exception("No device interfaces present.");
            }

            string devicePath = Marshal.PtrToStringAuto(new IntPtr(&devIFaceDetailData->devicePath));
            var hDriver = CreateFile(devicePath, FileAccess.ReadWrite, 0, IntPtr.Zero, FileMode.Open, 0, IntPtr.Zero);
            if (hDriver == (IntPtr)(-1))
            {
                Marshal.FreeHGlobal((IntPtr)devIFaceDetailData);
                throw new Exception("Error in CreateFile.");
            }
            return hDriver;
        }

        internal static IntPtr OpenKeyboardDevice()
        {
            return OpenDriverDevice(KBDRIVER_GUID);
        }
        internal static IntPtr OpenMouseDevice()
        {
            return OpenDriverDevice(MOUDRIVER_GUID);
        }

        internal static void CloseDriverHandle(IntPtr hDriver)
        {
            CloseHandle(hDriver);
        }
 
        internal static unsafe uint SendKeypressData (IntPtr hDriver, ushort scanCode, ushort flags) 
        {
            KeypressData kd = new KeypressData();
            kd.ScanCode = scanCode;
            kd.Flags = flags;

            uint retCode = 0;
            uint bytes = 0;
            DeviceIoControl(hDriver, IOCTL_KBFILTR_GEN_KEY_EVENT, (IntPtr)(&kd), SIZEOF_KEYPRESSDATA, (IntPtr)(&retCode), SIZEOF_UINT, out bytes, IntPtr.Zero);
            return retCode;
        }

        internal static unsafe uint SendMouseEventData(IntPtr hDriver, ushort buttonFlags, ushort buttonData, ushort flags, int lastX, int lastY)
        {
            MouseEventData md = new MouseEventData();
            md.ButtonFlags = buttonFlags;
            md.ButtonData = buttonData;
            md.Flags = flags;
            md.LastX = lastX;
            md.LastY = lastY;

            uint retCode = 0;
            uint bytes = 0;
            DeviceIoControl(hDriver, IOCTL_MOUFILTR_GEN_KEY_EVENT, (IntPtr)(&md), SIZEOF_MOUSEEVENTDATA, (IntPtr)(&retCode), SIZEOF_UINT, out bytes, IntPtr.Zero);
            return retCode;
        }
    }
}
