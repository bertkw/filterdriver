FILTER DRIVERS
==============

These filter drivers are derived from the ddk sample kbfiltr and moufiltr drivers.

At the moment, despite building .inf files, these drivers are best used as class filter drivers.  The installer batch file will copy the necessary files and install the necessary keys.  The uninstaller batch file will simple delete the files and remove the keys.  Both are tested and working.

WHY
===

These drivers allow you to generate keyboard and mouse input in a manner that is nearly indistinguishable from real keyboard and mouse events by client software.  This includes games, especially games that would otherwise balk at other forms of automation of user input.  *cough*.

CAVEATS
=======

As implemented, the driver is a potential security hole, as it allows any software that knows the driver's GUID and the driver's data packet layout to generate arbitrary keyboard and mouse events.

BUILDING
========

The solution FaKe (F# make) build file (build.fsx) currently doesn't work completely correctly.  The best way to build the solution is to use Visual Studio, and then use build.fsx to package the drivers:

1. Build solution in visual studio.  Currently, only the x64 Release Configuration is working; the driver is 64 bit and any 32 bit output binaries would be completely worthless.

2. `build.cmd package`

3. `build.cmd deploy`


INSTALLATION
============

1. Devcon.exe needs to be copied to c:\windows.  Test mode must be enabled using dseo.  The kbfiltr.cer must be installed (right click in explorer).  Ideally, the watermark remover should be used.

2. Run `install.bat` from elevated command prompt.

3. Reboot.

4. Run `diagnostics.bat` from an elevated command prompt to verify that the installation completed properly.  Both hidkeyboard and hidmouse should show up in their respective filter lists but not in the device lists.


To uninstall:

1. Run `uninstall.bat` from elevated command prompt.

2. Reboot.

3. Run `diagnostics.bat` from an elevated command prompt.  hidkeyboard and hidmouse should disappear from the filter lists.

INCLUDED PROJECTS
=================

Included projects in the solution under the "dotnet" folder are:

1. InputEventViewer, which is a simple F# Windows Forms app that displays a scrolling list of mouse and keyboard events caught by a global windows hook.  The source code contains a list of references regarding the global windows hooking technique.

2. OeMedia, which is a small C# library for interfacing with the two drivers.

3. OeMediaTestApp, which uses the OeMedia library to generate a sequence of keystrokes that, among other actions, opens notepad, sends a bunch of text, and opens a right-click context menu.  When run while the InputEventViewer is open, the generated events should all have an Injected value of "false".  Contrast this to events generated using AutoHotkey, which generates events with the Injected flag set to "true".

OTHER CREDITS
=============
As noted, these drivers were developed from the sample keyboard and mouse drivers in the windows ddk samples available on Github.
Some externally produced utilities are vital to installing and utilizing these drivers.  These tools include the dseo13b.exe tool (credit to ngohq.com) and the removewatermark tools (credit to deepxw).