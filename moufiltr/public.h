#ifndef _MOUFILTR_PUBLIC_H
#define _MOUFILTR_PUBLIC_H

#define IOCTL_INDEX_GEN_MOUSE_EVENT						0x812

#define IOCTL_MOUFILTR_GEN_KEY_EVENT		CTL_CODE( FILE_DEVICE_MOUSE,   \
                                                        IOCTL_INDEX_GEN_MOUSE_EVENT,    \
                                                        METHOD_BUFFERED,    \
                                                        FILE_ANY_ACCESS)

typedef struct MouseEvtData_t {
	USHORT ButtonFlags;
	USHORT ButtonData;
	USHORT Flags;
	int LastX;
	int LastY;
} MouseEvtData;
#endif
