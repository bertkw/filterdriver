/*++
Copyright (c) 2008  Microsoft Corporation

Module Name:

    moufiltr.h

Abstract:

    This module contains the common private declarations for the mouse
    packet filter

Environment:

    kernel mode only

Notes:


Revision History:


--*/

#ifndef MOUFILTER_H
#define MOUFILTER_H

#include <ntddk.h>
#include <kbdmou.h>
#include <ntddmou.h>
#include <initguid.h>
#include <devguid.h>
// #include <ntdd8042.h>
#include <wdf.h>
#define NTSTRSAFE_LIB
#include <ntstrsafe.h>

#include "public.h"

#if DBG

#define TRAP()                      DbgBreakPoint()

#define DebugPrint(_x_) DbgPrint _x_

#else   // DBG

#define TRAP()

#define DebugPrint(_x_)

#endif

 
typedef struct _DEVICE_EXTENSION
{
	WDFDEVICE WdfDevice;

	//
	// Queue for handling requests that come from the rawPdo
	//
	WDFQUEUE rawPdoQueue;

    // Previous hook routine and context
    //                               
    PVOID UpperContext;
     
    //
    // Context for IsrWritePort, QueueMousePacket
    //
    IN PVOID CallContext;

    //
    // The real connect data that this driver reports to
    //
    CONNECT_DATA UpperConnectData;

  
} DEVICE_EXTENSION, *PDEVICE_EXTENSION;

WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(DEVICE_EXTENSION,
	FilterGetData)

// {0CD9CEB0-3CDC-44B3-9CCC-65E5D7F4EDE4}
DEFINE_GUID(GUID_BUS_MOUFILTER,
	0x0CD9CEB0, 0x3CDC, 0x44B3, 0x9C, 0xCC, 0x65, 0xE5, 0xD7, 0xF4, 0xED, 0xE4);

// {DFAB6BB2-6E42-4F0E-9669-4C8167192D92}
DEFINE_GUID(GUID_DEVINTERFACE_MOUFILTER,
	0xDFAB6BB2, 0x6E42, 0x4F0E, 0x96, 0x69, 0x4C, 0x81, 0x67, 0x19, 0x2D, 0x92);

#define  MOUFILTR_DEVICE_ID L"{0CD9CEB0-3CDC-44B3-9CCC-65E5D7F4EDE4}\\OEMediaMouseDriver\0"
typedef struct _RPDO_DEVICE_DATA
{

	ULONG InstanceNo;

	//
	// Queue of the parent device we will forward requests to
	//
	WDFQUEUE ParentQueue;

} RPDO_DEVICE_DATA, *PRPDO_DEVICE_DATA;
WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(RPDO_DEVICE_DATA, PdoGetData)

//
// Prototypes
//
DRIVER_INITIALIZE DriverEntry;

EVT_WDF_DRIVER_DEVICE_ADD MouFilter_EvtDeviceAdd;
EVT_WDF_IO_QUEUE_IO_INTERNAL_DEVICE_CONTROL MouFilter_EvtIoInternalDeviceControl;

EVT_WDF_IO_QUEUE_IO_DEVICE_CONTROL MouFilter_EvtIoDeviceControlForRawPdo;
EVT_WDF_IO_QUEUE_IO_DEVICE_CONTROL MouFilter_EvtIoDeviceControlFromRawPdo;

NTSTATUS
MouFiltr_CreateRawPdo(
	WDFDEVICE       Device,
	ULONG           InstanceNo
);

VOID
MouFilter_DispatchPassThrough(
     _In_ WDFREQUEST Request,
    _In_ WDFIOTARGET Target
    );

VOID
MouFilter_ServiceCallback(
    IN PDEVICE_OBJECT DeviceObject,
    IN PMOUSE_INPUT_DATA InputDataStart,
    IN PMOUSE_INPUT_DATA InputDataEnd,
    IN OUT PULONG InputDataConsumed
    );

#endif  // MOUFILTER_H


